import cv2
import sys
import os
import numpy as np
from pykalman import KalmanFilter

def clamp(n, smallest, largest): 
    return max(smallest, min(n, largest))

video = cv2.VideoCapture('./ZOOM0004_0.mp4')

if (video.isOpened()== False): 
  print("Fehler beim öffnen vom Video!")
  sys.exit(1)

# Skip first 1000 frames
video.set(cv2.CAP_PROP_POS_FRAMES, 3000)
print("Done Skipping")

_,frame = video.read()
h, w, _ = frame.shape

videoOutput = cv2.VideoWriter('output.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (1920,1080))

# Kalman init
A = [
        [0.7,0,0,0,4,0,0,0],
        [0,0.7,0,0,0,4,0,0],
        [0,0,0.7,0,0,0,4,0],
        [0,0,0,0.7,0,0,0,4],
        [0,0,0,0,1,0,0,0],
        [0,0,0,0,0,1,0,0],
        [0,0,0,0,0,0,1,0],
        [0,0,0,0,0,0,0,1]]
C = [
        [1,0,0,0,0,0,0,0],
        [0,1,0,0,0,0,0,0],
        [0,0,1,0,0,0,0,0],
        [0,0,0,1,0,0,0,0]]
Q = 1.0e-5*np.eye(8)
R = 1.0e-1*np.eye(4)
kf = KalmanFilter(transition_matrices=A, observation_matrices=C, transition_covariance=Q, observation_covariance=R)
filtered_state_means = [0,0,0,0,0,0,0,0]
filtered_state_covariances = 1.0e-3*np.eye(8)

Measured = np.load("tracedFrames.npy")
ow = Measured[0][0]
oh = Measured[0][1]
index = 1
frameInPixel = 100
while(video.isOpened()):
  status, frame = video.read()
  if status == True:
    pred = Measured[index]
    pred_x_s = int(pred[0]/ow*w)
    pred_y_s = int(pred[1]/oh*h)
    pred_x_e = int(pred[2]/ow*w)
    pred_y_e = int(pred[3]/oh*h)

    # 
    filtered_x_s = pred_x_s 
    filtered_y_s = pred_y_s
    filtered_x_e = pred_x_e
    filtered_y_e = pred_y_e

    if filtered_x_s > filtered_x_e:
        b = filtered_x_s
        filtered_x_s = filtered_x_e
        filtered_x_e = b
    if filtered_y_s > filtered_y_e:
        b = filtered_y_s
        filtered_y_s = filtered_y_e
        filtered_y_e = b

    #16x9
    f_norm_y = int(abs(filtered_y_e-filtered_y_s)/9)
    f_norm_x = int(abs(filtered_x_e-filtered_x_s)/16)
    f_height = int(abs(filtered_y_e-filtered_y_s))
    f_width = int(abs(filtered_x_e-filtered_x_s))

    if f_height/9 >= f_width/16:
        f_center_x = abs(filtered_x_e-filtered_x_s)/2+filtered_x_s
        filtered_x_s = int(f_center_x-f_norm_y*16/2)
        filtered_x_e = int(f_center_x+f_norm_y*16/2)
    else:
        f_center_y = abs(filtered_y_e-filtered_y_s)/2+filtered_y_s
        filtered_y_s = int(f_center_y-f_norm_x*9/2)
        filtered_y_e = int(f_center_y+f_norm_x*9/2)

    # frame rois
    filtered_x_s -= frameInPixel
    filtered_y_s -= frameInPixel
    filtered_x_e += frameInPixel
    filtered_y_e += frameInPixel

    # clamp to image
    if filtered_x_s < 0:
        offset = (-1)*filtered_x_s
        filtered_x_s += offset
        filtered_x_e += offset
    
    if filtered_y_s < 0:
        offset = (-1)*filtered_y_s
        filtered_y_s += offset
        filtered_y_e += offset

    if filtered_x_e > w:
        offset = filtered_x_e-w
        filtered_x_s -= offset
        filtered_x_e -= offset

    if filtered_y_e > h:
        offset = filtered_y_e-h
        filtered_y_s -= offset
        filtered_y_e -= offset
    
    #cv2.rectangle(frame, (filtered_x_s,filtered_y_s), (filtered_x_e,filtered_y_e), (255, 0, 0), thickness=10)
    
    # Kalman-filter
    filtered_state_means, filtered_state_covariances = kf.filter_update(filtered_state_means, filtered_state_covariances, [filtered_x_s, filtered_y_s, filtered_x_e, filtered_y_e])
    filtered_x_s = int(filtered_state_means[0])
    filtered_y_s = int(filtered_state_means[1])
    filtered_x_e = int(filtered_state_means[2])
    filtered_y_e = int(filtered_state_means[3])
    
    #cv2.rectangle(frame, (filtered_x_s,filtered_y_s), (filtered_x_e,filtered_y_e), (0, 0, 255), thickness=10)
    crop_img = frame[np.maximum(0,filtered_y_s-frameInPixel):np.maximum(0,filtered_y_e+frameInPixel), np.maximum(0,filtered_x_s-frameInPixel):np.maximum(0,filtered_x_e+frameInPixel)]
    try:
        crop_img = cv2.resize(crop_img, (1920, 1080))
        videoOutput.write(crop_img)
        cv2.imshow("cropped", crop_img)
    except cv2.error as e:
        print(e)
    #cv2.imshow("output", frame)

    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
    index += 1
    if index >= 696:
        break
  else: 
    continue
 
video.release()
cv2.destroyAllWindows()
videoOutput.release()

print("DONE")

