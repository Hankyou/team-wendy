import pandas as pd
import numpy as np
from sklearn.cluster import KMeans

# width and height of image
width = 576
heigth = 324

# clusters: 9 for yolov3, 6 for yolov3-tiny
n_clusters = 6

# where to find the labels
DATABASE = "./rimondo_filtered.csv"
label_data = pd.read_csv(DATABASE)

# convert DataFrame to 2D-Array with only 'widths' and 'heights'
data = list(map(lambda i: [i[6]*width, i[7]*heigth], label_data.values.tolist()))

# find most promenent sizes with k-means
clusters = KMeans(n_clusters=n_clusters, random_state=1337).fit(data)
centers = np.array(clusters.cluster_centers_)
centers = list(map(lambda i: [int(i[0]), int(i[1])], centers))

for center in centers:
  print(center)