import random
import os
from os.path import join
import sys

train2testRatio = 0.8

frames = [f for f in os.listdir("./images") if f.endswith(".jpg")]

train = open("train.txt", "w+")
test = open("test.txt", "w+")

framesArray = []

for frame in frames:
  framesArray.append(frame)

random.shuffle(framesArray)

trainCount = int(len(framesArray)*train2testRatio)

for i in range(0, trainCount):
  train.write("./images/"+framesArray[i]+"\n")

for i in range(trainCount, len(framesArray)):
  test.write("./images/"+framesArray[i]+"\n")

train.close()
test.close()