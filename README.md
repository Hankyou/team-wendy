# Team-Wendy

## Links
* [YOLO: Real-Time Object Detection](https://pjreddie.com/darknet/yolo/)
* [Tutorial: Build your custom real-time object classifier](https://towardsdatascience.com/tutorial-build-an-object-detection-system-using-yolo-9a930513643a#e8dd)
* [Read, Write and Display a video using OpenCV ( C++/ Python )](https://www.learnopencv.com/read-write-and-display-a-video-using-opencv-cpp-python/)
* [YOLOv3-Darknet-PythonWrapper](https://github.com/madhawav/YOLO3-4-Py)
  * export DARKNET_HOME="PATH_TO_DARKNET_INSTALLATION"
  * export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:$DARKNET_HOME