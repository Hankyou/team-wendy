# this is a small example how to read the labels for the rimondo dataset using pandas
# and identifying the corresponding rectangles
# author: soeren.klemm@uni-muenster.de
# date: 19. Nov. 2019
import os
import pandas as pd
import sys
import statistics as st
import cv2

from os.path import join
from PIL import Image, ImageDraw

# where to find the frames, must contain subfolders 'GOPR*' and 'GP0*'
FRAME_SOURCE = "./rimondo_frames"
# where to find the labels
DATABASE = "./rimondo_filtered.csv"
# colors used for drawing rectangels
colors = {1: "red", 2: "lightgreen"}

label_data = pd.read_csv(DATABASE)

# names of all videos
videos = [d for d in os.listdir(FRAME_SOURCE) if os.path.isdir(join(FRAME_SOURCE, d))]

def resizeImage(img, factor):
  h, w, _ = img.shape
  return cv2.resize(img, (int(w*factor),int(h*factor)))

# 
scaleFactor = 0.15
label_img_pairs = 0

for vid in videos:
    video_path = join(FRAME_SOURCE, vid)
    frames = [f for f in os.listdir(video_path) if f.endswith(".png")]

    for frame in frames:
        # images were stored as jpg on the server but are (lossless) png now
        id = vid + "/" + frame.replace("png", "jpg")
        image_file = join(video_path, frame)

        # Read in image
        img = cv2.imread(image_file)
        if img is None:
            continue

        # resize image
        smallerImage = resizeImage(img, scaleFactor)

        all_rects_horse = label_data.loc[
            (label_data["image"] == id) & (label_data["label"] == 1)
        ]

        all_rects_human = label_data.loc[
            (label_data["image"] == id) & (label_data["label"] == 2)
        ]

        if len(all_rects_horse) > 0 or len(all_rects_human):
            it_horse = all_rects_horse.iterrows()
            it_human = all_rects_human.iterrows()
            while 1:
                indexho, horse = 0, None
                try:
                    indexho, horse = next(it_horse)
                except StopIteration:
                    pass
                
                indexhu, human = 0, None
                try:
                    indexhu, human = next(it_human)
                except StopIteration:
                    pass

                if indexhu != 0 or indexho != 0:
                    label_img_pairs += 1
                    # save image
                    newImagePath = "./images/"+vid+"_"+str(indexho)+"_"+str(indexhu)+"_"+frame.replace("png", "jpg")
                    cv2.imwrite(newImagePath, smallerImage)

                    # save label(s)
                    lableFile = "./labels/"+vid+"_"+str(indexho)+"_"+str(indexhu)+"_"+frame.replace("png", "txt")
                    labelFile = open(lableFile, "w+")
                    if indexho != 0:
                        labelFile.write("0 "+str(horse["x"])+" "+str(horse["y"])+" "+str(horse["width"])+" "+str(horse["height"])+"\n")
                    if indexhu != 0:
                        labelFile.write("1 "+str(human["x"])+" "+str(human["y"])+" "+str(human["width"])+" "+str(human["height"])+"\n")
                    labelFile.close()
                else:
                    break

print("label_img_pairs: ", label_img_pairs)
print("DONE")
