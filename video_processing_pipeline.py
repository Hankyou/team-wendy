import cv2
import sys
import os
import numpy as np
from pydarknet import Detector, Image

video = cv2.VideoCapture('./ZOOM0004_0.mp4')

if (video.isOpened()== False): 
  print("Fehler beim öffnen vom Video!")
  sys.exit(1)

def resizeImage(img, factor):
  h, w, _ = img.shape
  return cv2.resize(img, (int(w*factor),int(h*factor)))

scaleFactor = 0.15

## yolov3-tiny
#net = Detector(bytes("configs/cfg/yolov3-tiny.cfg", encoding="utf-8"), 
#  bytes("configs/obj_126000.weights", encoding="utf-8"), 0, 
#  bytes("configs/obj.data",encoding="utf-8"))

# yolov3
net = Detector(bytes("configs/cfg/yolov3.cfg", encoding="utf-8"), 
  bytes("configs/yolov3_Z_7000.weights", encoding="utf-8"), 0, 
  bytes("configs/obj.data",encoding="utf-8"))

# Skip first 1000 frames
video.set(cv2.CAP_PROP_POS_FRAMES, 3000)
print("Done Skipping")

_,frame = video.read()
frame = resizeImage(frame, scaleFactor)
h, w, _ = frame.shape
videoOutput = cv2.VideoWriter('output.avi',cv2.VideoWriter_fourcc('M','J','P','G'), 10, (w,h))

data = np.array([[w, h, 0, 0]])

while(video.isOpened()):
  #for i in range(0, 10):
  #    _,_ = video.read()
  #    cv2.waitKey(10)
  status, frame = video.read()
  if status == True:
    frame = resizeImage(frame, scaleFactor)

    img2 = Image(frame)
    results = net.detect(img2, 0.05, 0.1)
    smallest_x_s = 1000
    smallest_y_s = 1000
    biggest_x_e = -1
    biggest_y_e = -1
    for cat, score, bounds in results:
        x, y, w, h = bounds
        x_s = int(x - w / 2)
        y_s = int(y - h / 2)
        x_e = int(x + w / 2)
        y_e = int(y + h / 2)

        if smallest_x_s > x_s:
          smallest_x_s = x_s
        if smallest_y_s > y_s:
          smallest_y_s = y_s

        if biggest_x_e < x_e:
          biggest_x_e = x_e
        if biggest_y_e < y_e:
          biggest_y_e = y_e

        color = (0, 0, 0)
        if cat == b"human":
          color = (0, 0, 255)
        elif cat == b"horse":
          color = (255, 0, 0)
        
        cv2.rectangle(frame, (x_s, y_s), (x_e, y_e), color, thickness=2)
    
    data = np.append(data, [[smallest_x_s, smallest_y_s, biggest_x_e, biggest_y_e]], axis=0)    
    cv2.rectangle(frame, (smallest_x_s - 10, smallest_y_s - 10), (biggest_x_e + 10, biggest_y_e + 10), (0, 155, 0), thickness=2)
        
    videoOutput.write(frame)
    cv2.imshow("output", frame)

    if cv2.waitKey(10) & 0xFF == ord('q'):
      break
  else: 
    continue
 
video.release()
cv2.destroyAllWindows()
np.save("tracedFrames", data)

print("DONE")

